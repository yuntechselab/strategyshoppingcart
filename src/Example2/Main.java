package Example2;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ShoppingCart sp = new ShoppingCart();
		sp.setPaymentStrategy(new CashStrategy());
		sp.pay();
		
		
		sp.setPaymentStrategy(new CreditCardStrategy());
		sp.pay();
		
		
		sp.setPaymentStrategy(new PayPalStrategy());
		sp.pay();
	}
}
