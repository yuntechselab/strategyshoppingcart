package Example2;

public class ShoppingCart {
	private PaymentStrategy paymentStrategy;

	public void setPaymentStrategy(PaymentStrategy paymentStrategy){this.paymentStrategy = paymentStrategy;}
	
	public void pay() {
		paymentStrategy.pay();
	}
}
