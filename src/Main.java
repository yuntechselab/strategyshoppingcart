//original example
//http://www.journaldev.com/1754/strategy-design-pattern-in-java-example-tutorial

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ShoppingCart shoppingCart = new ShoppingCart(100);
		
		shoppingCart.pay(new CashStrategy());
		shoppingCart.pay(new CreditCardStrategy());

	}

}
