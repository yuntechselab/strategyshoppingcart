
public class ShoppingCart {
	int amount;
	public ShoppingCart(int amount){this.amount = amount;}
	public void pay(PaymentStrategy paymentStrategy){
		paymentStrategy.pay(amount);
	}
}
